/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('deepmerge');
const wdioConf = require('./wdio.conf.js');

// have main config file as default but overwrite environment specific information
exports.config = merge(wdioConf.config, {
  baseUrl: '',
  hostname: 'selenium-firefox',
  // Use headless browser for CI testing on Gitlab
  capabilities: [
    {
      maxInstances: 1,
      browserName: 'firefox',
      browserVersion: '75.0', // browser version
      platformName: 'linux', // OS platform
      'moz:firefoxOptions': {
        // flag to activate Firefox headless mode (see https://github.com/mozilla/geckodriver/blob/master/README.md#firefox-capabilities for more details about moz:firefoxOptions)
        args: ['-headless'],
      },
    },
  ],
});
