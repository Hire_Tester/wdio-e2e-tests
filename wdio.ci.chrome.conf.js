/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('deepmerge');
const wdioConf = require('./wdio.conf.js');

// have main config file as default but overwrite environment specific information
exports.config = merge(wdioConf.config, {
  baseUrl: '',
  hostname: 'selenium-chrome',
  // Use headless browser for CI testing on Gitlab
  capabilities: [
    {
      maxInstances: 1,
      browserName: 'chrome',
      browserVersion: '81.0.4044.92', // browser version
      platformName: 'linux', // OS platform
      'goog:chromeOptions': {
        args: ['--headless', '--disable-gpu'],
      },
    },
  ],
});
