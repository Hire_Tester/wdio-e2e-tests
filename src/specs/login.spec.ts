import { expect } from 'chai';
import LoginPage from '../pages/LoginPage';
import allureReporter from '@wdio/allure-reporter';
import { routes, superAdmin, unknownUser } from '../config/constants';

describe('Login Page - Admin User', () => {
  before(() => {
    allureReporter.addStep('Navigate to the Login Page');
    LoginPage.open(routes.login);
  });

  after(() => {
    allureReporter.addStep('Logout from the application');
    LoginPage.logout();
  });

  it('should allow access with correct creds', () => {
    allureReporter.addStep('Authentification as admin');
    allureReporter.addArgument('Email: ', superAdmin.email);
    allureReporter.addArgument('Password: ', superAdmin.password);
    LoginPage.loginAsCustomer(superAdmin.email, superAdmin.password);
    const text = LoginPage.getUserName().toLowerCase();
    expect(text).to.equal('superadmin');
  });
});

describe('Login Page - Unknown User', () => {
  before(() => {
    allureReporter.addStep('Navigate to the Login Page');
    LoginPage.open(routes.login);
  });

  it('should deny access with wrong creds', () => {
    allureReporter.addStep('Authentification as unregistered user');
    allureReporter.addArgument('Email: ', unknownUser.email);
    allureReporter.addArgument('Password: ', unknownUser.password);
    LoginPage.loginAsCustomer(unknownUser.email, unknownUser.password);
    const status = LoginPage.getErrorStatus();
    expect(status).to.be.true;
  });
});
