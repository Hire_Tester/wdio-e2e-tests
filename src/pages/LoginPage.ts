import BasePage from './BasePage';
import { Element } from '@wdio/sync';

class LoginPage extends BasePage {
  /**
   * define elements
   */
  get emailInput(): Element {
    return $("[aria-label='E-mail']");
  }

  get passwordInput(): Element {
    return $("[aria-label='Password']");
  }

  get loginBtn(): Element {
    return $('button[type=button]');
  }

  get userName(): Element {
    return $('.v-toolbar__items button');
  }

  get errorMsg(): Element {
    return $('.v-messages__message');
  }

  get logoutBtn(): Element {
    return $("//a[contains(@class, 'v-list__tile')]/div[contains(., 'Logout')]");
  }

  /**
   * define or overwrite page methods
   */
  public loginAsCustomer(email: string, password: string): void {
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
  }

  public getUserName(): string {
    return this.userName.getText();
  }

  public logout(): void {
    this.userName.click();
    this.logoutBtn.click();
  }

  public getErrorMsg(): string {
    this.errorMsg.waitForDisplayed(2000);
    return this.errorMsg.getText();
  }

  public getErrorStatus(): boolean {
    return this.errorMsg.isDisplayed();
  }
}

export default new LoginPage();
