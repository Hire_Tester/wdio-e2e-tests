export default abstract class BasePage {
  private _path: string;

  public open(path: string): void {
    this._path = path;
    browser.maximizeWindow();
    browser.url(path);
  }

  public getTitle(): string {
    return browser.getTitle();
  }
}
