export const routes = {
  login: '#/login',
  users: '#/users/list',
  addUser: '#/users/create',
};

export const superAdmin = {
  email: '',
  password: '',
};

export const unknownUser = {
  email: '',
  password: '',
};
