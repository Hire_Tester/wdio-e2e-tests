# E2E testing web app with WebdriverIO
Automated testing of web user interface (Admin Panel).
## Getting Started
The main purpose is creating automated tests that are resistant to changes in the UI.
## Setup
1. Install the latest version of [node.js](https://nodejs.org/). Once the install completes, test that node is installed by typing the following into the terminal `node -v && npm -v`. If you've got Node/npm already installed, you should update them to their latest versions. To update Node, the most reliable way is to download and install an updated installer package from their website (see link above). To update npm `npm install npm@latest -g`.
2. Install JDK 1.8 for running allure report.
3. Clone this repository and open the folder with project.
4. Install dependencies `npm install`.
5. Run tests execute `npm test`.
6. Run `npm run report` to build `allure` report from results.

## Why WebdriverIO?
[WDIO](https://webdriver.io/) is a custom implementation of W3C webdriver API. This gives it an edge to have full control over implementation rather than depending on WebdriverJS implementation. It is a good framework for any type of applications, including mobile (via Appium). The readability is good thanks to no need of using async/await and laconic waiters. It has a good support and enthusiastic developer community.

## Install WebdriverIO CLI
```javascript
npm i -D @wdio/cli
```

## Configuration
Next, you’ll generate a configuration file to store your WebdriverIO settings.
To do that, just run the configuration utility:
```javascript
./node_modules/.bin/wdio config -y
```

The configurator will install all required packages for you and create a config file called wdio.conf.js.

## Typescript Setup
You can register *Typescript* to compile your ***.spec.ts** files in the **before** hook of your coding file. You will need *ts-node* and *tsconfig-paths* installed as devDependencies.
The minimum TypeScript version is 3.5.1.
```js
// wdio.conf.js
before: function() {
    require('ts-node').register({ files: true })
},
```
Similarly for Mocha:
``` JS
// wdio.conf.js
mochaOpts: {
    ui: 'bdd',
    require: [
        'tsconfig-paths/register'
    ]
},
```

## JUnit Reporter
> A WebdriverIO reporter that creates Gitlab CI compatible XML based reports.

This reporter will output a report for each runner, so in turn you will receive an xml report for each spec file. The report is placed in **e2e/reports/junit-report** folder.

### Installation
``` JS
npm i -D @wdio/junit-reporter
```

### Configuration
``` JS
reporters: [
  //..
  ['junit', {
    outputDir: './',
    outputFileFormat: () => 'e2e-results.xml'
  }]
  //..
],
```

## Spec Reporter
> A WebdriverIO plugin to report in spec style.

### Installation
``` JS
npm i -D @wdio/spec-reporter
```

### Configuration
``` JS
// wdio.conf.js
//...
reporters: ['spec'],
//..
```

You can see the results for each runner in a console output.

## Allure Reporter
> A WebdriverIO reporter plugin to create [Allure Test Reports](https://docs.qameta.io/allure/)

<p align="center">
  <img src="http://i.imgur.com/HtBBeS3.png">
</p>

### Installation
``` JS
npm i -D @wdio/allure-reporter@5.16.0
```

** Allure doesn't correctly report **'passing'** tests starting with *@wdio/allure-reporter version* **5.16.10**

### Configuration
``` JS
// wdio.conf.js
reporters: [
  'spec',
  ['allure', {
    outputDir: 'allure-results',
    disableWebdriverStepsReporting: true,
  }],
],
```

### Usage
Allure API can be accessed using:
``` JS
import allureReporter from "@wdio/allure-reporter";
```

> [Supported Allure API](https://webdriver.io/docs/allure-reporter.html#supported-allure-api)

### Displaying the report

The results can be consumed by any of the [reporting tools](https://docs.qameta.io/allure/#_reporting) offered by Allure. For example:</br>
#### Command-line

Install the [Allure command-line tool](https://www.npmjs.com/package/allure-commandline), and run the next command:

``` JS
npm run report
```

This will generate a report (by default in **./allure-report**), and open it in your browser.

### Add Screenshots
Screenshots can be attached to the report by using the **takeScreenshot** function from WebDriverIO in **afterStep** hook. First set *disableWebdriverScreenshotsReporting: false* in reporter options, then add in **afterStep** hook
``` JS
// wdio.conf.js
afterTest: function (test) {
  if (test.error !== undefined) {
    browser.takeScreenshot();
  }
},
```


## Testing Home Page using Page Object Pattern
The goal of using page objects is to abstract any page information away from the actual tests. Ideally, you should store all selectors or specific instructions that are unique for a certain page in a page object, so that you still can run your test after you've completely redesigned your page.

### Making a Page Object
First off, we need a main page that we call BasePage. It will contain general selectors or methods which all page objects will inherit from.
``` JS
export default abstract class BasePage {
  private _path: string;

  public open(path: string): void {
    this._path = path;
    browser.maximizeWindow();
    browser.url(path);
  }

  public getTitle(): string {
    return browser.getTitle();
  }
}
```

Then we need to build a page object for the home page.
The first step is to write all important selectors that are required in our **HomePage** object as getters functions:
``` JS
import BasePage from './BasePage';
import { Element } from '@wdio/sync';

class LoginPage extends BasePage {
  /**
   * define elements
   */
  get emailInput(): Element {
    return $("[aria-label='E-mail']");
  }

  get passwordInput(): Element {
    return $("[aria-label='Password']");
  }

  get loginBtn(): Element {
    return $('button[type=button]');
  }

  get userName(): Element {
    return $('.v-toolbar__items button');
  }

  get errorMsg(): Element {
    return $('.v-messages__message');
  }

  get logoutBtn(): Element {
    return $("//a[contains(@class, 'v-list__tile')]/div[contains(., 'Logout')]");
  }

  /**
   * define or overwrite page methods
   */
  public loginAsCustomer(email: string, password: string): void {
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
  }

  public getUserName(): string {
    return this.userName.getText();
  }

  public logout(): void {
    this.userName.click();
    this.logoutBtn.click();
  }

  public getErrorMsg(): string {
    this.errorMsg.waitForDisplayed(2000);
    return this.errorMsg.getText();
  }

  public getErrorStatus(): boolean {
    return this.errorMsg.isDisplayed();
  }
}

export default new LoginPage();
```

These functions are evaluated when you access the property, not when you generate the object.

## Using Page Objects in Tests
After you've defined the necessary elements and methods for the page, you can start to write the test for it.

``` JS
import { expect } from 'chai';
import LoginPage from '../pages/LoginPage';
import allureReporter from '@wdio/allure-reporter';
import { routes, superAdmin, unknownUser } from '../config/constants';

describe('Login Page - Admin User', () => {
  before(() => {
    allureReporter.addStep('Navigate to the Login Page');
    LoginPage.open(routes.login);
  });

  after(() => {
    allureReporter.addStep('Logout from the application');
    LoginPage.logout();
  });

  it('should allow access with correct creds', () => {
    allureReporter.addStep('Authentification as admin');
    allureReporter.addArgument('Email: ', superAdmin.email);
    allureReporter.addArgument('Password: ', superAdmin.password);
    LoginPage.loginAsCustomer(superAdmin.email, superAdmin.password);
    const text = LoginPage.getUserName().toLowerCase();
    expect(text).to.equal('superadmin');
  });
});

describe('Login Page - Unknown User', () => {
  before(() => {
    allureReporter.addStep('Navigate to the Login Page');
    LoginPage.open(routes.login);
  });

  it('should deny access with wrong creds', () => {
    allureReporter.addStep('Authentification as unregistered user');
    allureReporter.addArgument('Email: ', unknownUser.email);
    allureReporter.addArgument('Password: ', unknownUser.password);
    LoginPage.loginAsCustomer(unknownUser.email, unknownUser.password);
    const status = LoginPage.getErrorStatus();
    expect(status).to.be.true;
  });
});
```

## Run Tests Locally
Finally, to run tests use:
``` JS
npm test
```
