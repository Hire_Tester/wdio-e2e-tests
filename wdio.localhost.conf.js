/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('deepmerge');
const wdioConf = require('./wdio.conf.js');

// have main config file as default but overwrite environment specific information
exports.config = merge(wdioConf.config, {
  baseUrl: '',
  capabilities: [
    {
      maxInstances: 1,
      browserName: 'firefox', // options: `firefox`, `chrome`, `opera`
      browserVersion: '75.0', // browser version
      platformName: 'linux', // OS platform
    },
  ],
});
